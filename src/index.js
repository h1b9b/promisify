/**
 * promisify()
 * Transforms a callback function -- func(arg1, arg2 .. argN, callback) --
 * into a Promise.
 *
 * @param {function} func - The function to promisify
 * @return {function} A promisified version of `func`
 */
function promisify(func) {
  // If agrument is not a function, throw error
  if (typeof func !== 'function') {
    throw new TypeError('Argument to promisify must be a function');
  }

  // If the user has supplied a custom Promise implementation, use it. Otherwise
  // fall back to available Promise in the global scope.
  const PromiseObject = Promisify.Promise || Promise;

  // If no Promise implemention found, throw error
  if (typeof PromiseObject !== 'function') {
    throw new Error('No Promise implementation found');
  }

  return function(...args) {
    return new PromiseObject((resolve, reject) => {
      // Append the callback bound to the context
      args.push(function callback(err, ...values) {
        // If there is an error, it's rejected
        if (err) reject(err);
        // If only one argment in callback function, we resole with it
        if (values.length === 1) resolve(values[0]);
        // Else the porimse is resolved with all the argments
        return resolve(...values);
      });
      // Call the function.
      func.call(this, ...args);
    });
  };
}

// Set Promise to undefined to use the default implementation
// of Promise available
const Promisify = {
  Promise: undefined,
  promisify
};

export default Promisify;
