const Promisify = require('../index').default;
const { promisify } = Promisify;

describe('promisify', () => {
  function namedFunction(success, callback) {
    if (success === false) return callback('error');
    return callback(undefined, 'success');
  }

  it('should resolve with string "success"', async () => {
    expect.assertions(1);
    const promisified = promisify(namedFunction);
    const result = await promisified(true);
    expect(result).toBe('success');
  });

  it('should reject with string "error"', async () => {
    expect.assertions(1);
    try {
      const promisified = promisify(namedFunction);
      await promisified(false);
    } catch (error) {
      expect(error).toBe('error');
    }
  });

  it('should throw type error when argument not func', () => {
    expect.assertions(2);
    try {
      promisify();
    } catch (error) {
      expect(error.name).toBe('TypeError');
      expect(error.message).toBe('Argument to promisify must be a function');
    }
  });

  it('should throw error when Promise implementation not defined', () => {
    expect.assertions(2);
    try {
      Promisify.Promise = {};
      promisify(namedFunction);
    } catch (error) {
      expect(error.name).toBe('Error');
      expect(error.message).toBe('No Promise implementation found');
    }
  });
});
